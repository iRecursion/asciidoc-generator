echo off
cls
chcp 65001

set BASE_URL=D:/Install/Module/Antora

set ANTORA_URL=%BASE_URL%/asciidoc-generator/
set SITE_URL=%BASE_URL%/asciidoc-distro/docs/index.html

d:
cd %ANTORA_URL%

rem --extension @antora/pdf-extension
rem --extension @antora/lunr-extension

rem -r asciidoctor-pdf
rem -r @djencks/asciidoctor-mathjax

rem --fetch antora-playbook.yml

antora antora-playbook.yml & start %SITE_URL%
